"""
Stwórz program, który na podstawie

tabeli inflacji wartości
oprocentowania kredytu,
kwoty początkowej kredytu
stałej wartości raty
wyliczy wartość zadłużenia w każdym miesiącu przez 2 nadchodzące lata.

Niech program wydrukuje dla każdego miesiąca następującą linię:
Twoja pozostała kwota kredytu to X, to Y mniej niż w poprzednim miesiącu.

Napisz program tak, by wysokość początkowego kredytu, oprocentowanie kredytu (ponad inflację) i kwota raty były pobierane ze standardowego wejścia (terminal).

Przykładowe wartości kredytu i formułę do jego wyliczenia znajdziesz w załączniku powyżej. Skopiuj z niego wartości inflacji dla każdego miesiąca.

"""
initial_credit_value = input("podaj wysokosc kredytu: ")
credit_perc = input("Oprocentowanie kredytu: ")
rate_value = input("Kwota raty: ")

initial_credit_value = float(initial_credit_value)
credit_perc = float(credit_perc)
rate_value = float(rate_value)

inflation_1 = 1.59282448436825
inflation_2 = -0.453509101198007
inflation_3 = 2.32467171712441
inflation_4 = 1.26125440724877
inflation_5 = 1.78252628571251
inflation_6 = 2.32938454145522
inflation_7 = 1.50222984223283
inflation_8 = 1.78252628571251
inflation_9 = 2.32884899407637
inflation_10 = 0.616921348207244
inflation_11 = 2.35229588637833
inflation_12 = 0.337779545187098
inflation_13 = 1.57703524727525
inflation_14 = -0.292781442607648
inflation_15 = 2.48619659017508
inflation_16 = 0.267110317834564
inflation_17 = 1.41795267229799
inflation_18 = 1.05424326726375
inflation_19 = 1.4805201044812
inflation_20 = 1.57703524727525
inflation_21 = -0.0774206903147018
inflation_22 = 1.16573339872354
inflation_23 = -0.404186717638335
inflation_24 = 1.49970852083123

# =(1 + ((B3+3)/1200)) * C2 - 200
# 3% oprocentowanie
# rata 200
# STYCZEN
credit_value_1 = (1 + ((inflation_1+credit_perc)/1200))*initial_credit_value-rate_value
print(credit_value_1)
# Twoja pozostała kwota kredytu to X, to Y mniej niż w poprzednim miesiącu
print(f"Twoja pozostała kwota kredytu to {credit_value_1:.2f}, to {initial_credit_value - credit_value_1:.2f} mniej niż w poprzednim miesiącu.")
# LUTY
credit_value_2 = (1 + ((inflation_2+credit_perc)/1200))*credit_value_1-rate_value
#credit_value_2_real = round(credit_value_2, 2)
print(credit_value_2)
#print(f"Twoja pozostała kwota kredytu to {credit_value_2_real}, to {credit_value_1 - credit_value_2} mniej niż w poprzednim miesiącu")
print(f"Twoja pozostała kwota kredytu to {credit_value_2:.2f}, to {credit_value_1 - credit_value_2:.2f} mniej niż w poprzednim miesiącu.")
# MARZEC
credit_value_3 = (1 + ((inflation_3+credit_perc)/1200))*credit_value_2-rate_value
credit_value_3_real = round(credit_value_3, 2)
print(credit_value_3)
print(f"Twoja pozostała kwota kredytu to {credit_value_3_real}, to {credit_value_2 - credit_value_3:.2f} mniej niż w poprzednim miesiącu.")
# KWIECIEN
credit_value_4 = (1 + ((inflation_4+credit_perc)/1200))*credit_value_3-rate_value
credit_value_4_real = round(credit_value_4, 2)
print(credit_value_4)
print(f"Twoja pozostała kwota kredytu to {credit_value_4_real}, to {credit_value_3 - credit_value_4:.2f} mniej niż w poprzednim miesiącu.")
# MAJ
credit_value_5 = (1 + ((inflation_5+credit_perc)/1200))*credit_value_4-rate_value
credit_value_5_real = round(credit_value_5, 2)
print(f"MAJ {credit_value_5:.2f}")
print(f"Twoja pozostała kwota kredytu to {credit_value_5_real}, to {credit_value_4 - credit_value_5:.2f} mniej niż w poprzednim miesiącu.")
# CZERWIEC
credit_value_6 = (1 + ((inflation_6+credit_perc)/1200))*credit_value_5-rate_value
print(f"CZERWIEC {credit_value_6:.2f}")
print(f"Twoja pozostała kwota kredytu to {credit_value_6:.2f}, to {credit_value_5 - credit_value_6:.2f} mniej niż w poprzednim miesiącu.")
# LIPIEC
credit_value_7 = (1 + ((inflation_7+credit_perc)/1200))*credit_value_6-rate_value
print(f"LIPIEC {credit_value_7:.2f}")
print(f"Twoja pozostała kwota kredytu to {credit_value_7:.2f}, to {credit_value_6 - credit_value_7:.2f} mniej niż w poprzednim miesiącu.")
# SIERPIEN
credit_value_8 = (1 + ((inflation_8+credit_perc)/1200))*credit_value_7-rate_value
print(f"SIERPIEN {credit_value_8:.2f}")
print(f"Twoja pozostała kwota kredytu to {credit_value_8:.2f}, to {credit_value_7 - credit_value_8:.2f} mniej niż w poprzednim miesiącu.")
# WRZESIEN
credit_value_9 = (1 + ((inflation_9+credit_perc)/1200))*credit_value_8-rate_value
print(f"WRZESIEN {credit_value_9:.2f}")
print(f"Twoja pozostała kwota kredytu to {credit_value_9:.2f}, to {credit_value_8 - credit_value_9:.2f} mniej niż w poprzednim miesiącu.")
# PAZDZIERNIK
credit_value_10 = (1 + ((inflation_10+credit_perc)/1200))*credit_value_9-rate_value
print(f"PAZDZIERNIK {credit_value_10:.2f}")
print(f"Twoja pozostała kwota kredytu to {credit_value_10:.2f}, to {credit_value_9 - credit_value_10:.2f} mniej niż w poprzednim miesiącu.")
# LISTOPAD
credit_value_11 = (1 + ((inflation_11+credit_perc)/1200))*credit_value_10-rate_value
print(f"LISTOPAD {credit_value_11:.2f}")
print(f"Twoja pozostała kwota kredytu to {credit_value_11:.2f}, to {credit_value_10 - credit_value_11:.2f} mniej niż w poprzednim miesiącu.")
# GRUDZIEN
credit_value_12 = (1 + ((inflation_12+credit_perc)/1200))*credit_value_11-rate_value
print(f"GRUDZIEN {credit_value_12:.2f}")
print(f"Twoja pozostała kwota kredytu to {credit_value_12:.2f}, to {credit_value_11 - credit_value_12:.2f} mniej niż w poprzednim miesiącu.")
# STYCZEN
credit_value_13 = (1 + ((inflation_13+credit_perc)/1200))*credit_value_12-rate_value
print(f"STYCZEN {credit_value_13:.2f}")
print(f"Twoja pozostała kwota kredytu to {credit_value_13:.2f}, to {credit_value_12 - credit_value_13:.2f} mniej niż w poprzednim miesiącu.")
# LUTY
credit_value_14 = (1 + ((inflation_14+credit_perc)/1200))*credit_value_13-rate_value
print(f"LUTY {credit_value_14:.2f}")
print(f"Twoja pozostała kwota kredytu to {credit_value_14:.2f}, to {credit_value_13 - credit_value_14:.2f} mniej niż w poprzednim miesiącu.")
# MARZEC
credit_value_15 = (1 + ((inflation_15+credit_perc)/1200))*credit_value_14-rate_value
print(f"MARZEC {credit_value_15:.2f}")
print(f"Twoja pozostała kwota kredytu to {credit_value_15:.2f}, to {credit_value_14 - credit_value_15:.2f} mniej niż w poprzednim miesiącu.")
# KWIECIEN
credit_value_16 = (1 + ((inflation_16+credit_perc)/1200))*credit_value_15-rate_value
print(f"KWIECIEN {credit_value_16:.2f}")
print(f"Twoja pozostała kwota kredytu to {credit_value_16:.2f}, to {credit_value_15 - credit_value_16:.2f} mniej niż w poprzednim miesiącu.")
# MAJ
credit_value_17 = (1 + ((inflation_17+credit_perc)/1200))*credit_value_16-rate_value
print(f"MAJ {credit_value_17:.2f}")
print(f"Twoja pozostała kwota kredytu to {credit_value_17:.2f}, to {credit_value_16 - credit_value_17:.2f} mniej niż w poprzednim miesiącu.")
# CZERWIEC
credit_value_18 = (1 + ((inflation_18+credit_perc)/1200))*credit_value_17-rate_value
print(f"CZERWIEC {credit_value_18:.2f}")
print(f"Twoja pozostała kwota kredytu to {credit_value_18:.2f}, to {credit_value_17 - credit_value_18:.2f} mniej niż w poprzednim miesiącu.")
# LIPIEC
credit_value_19 = (1 + ((inflation_19+credit_perc)/1200))*credit_value_18-rate_value
print(f"LIPIEC {credit_value_19:.2f}")
print(f"Twoja pozostała kwota kredytu to {credit_value_19:.2f}, to {credit_value_18 - credit_value_19:.2f} mniej niż w poprzednim miesiącu.")
# SIERPIEN
credit_value_20 = (1 + ((inflation_20+credit_perc)/1200))*credit_value_19-rate_value
print(f"SIERPIEN {credit_value_20:.2f}")
print(f"Twoja pozostała kwota kredytu to {credit_value_20:.2f}, to {credit_value_19 - credit_value_20:.2f} mniej niż w poprzednim miesiącu.")
# WRZESIEN
credit_value_21 = (1 + ((inflation_21+credit_perc)/1200))*credit_value_20-rate_value
print(f"WRZESIEN {credit_value_21:.2f}")
print(f"Twoja pozostała kwota kredytu to {credit_value_21:.2f}, to {credit_value_20 - credit_value_21:.2f} mniej niż w poprzednim miesiącu.")
# PAZDZIERNIK
credit_value_22 = (1 + ((inflation_22+credit_perc)/1200))*credit_value_21-rate_value
print(f"PAZDZIERNIK {credit_value_22:.2f}")
print(f"Twoja pozostała kwota kredytu to {credit_value_22:.2f}, to {credit_value_21 - credit_value_22:.2f} mniej niż w poprzednim miesiącu.")
# LISTOPAD
credit_value_23 = (1 + ((inflation_23+credit_perc)/1200))*credit_value_22-rate_value
print(f"LISTOPAD {credit_value_23:.2f}")
print(f"Twoja pozostała kwota kredytu to {credit_value_23:.2f}, to {credit_value_22 - credit_value_23:.2f} mniej niż w poprzednim miesiącu.")
# GRUDZIEN
credit_value_24 = (1 + ((inflation_24+credit_perc)/1200))*credit_value_23-rate_value
print(f"GRUDZIEN {credit_value_24:.2f}")
print(f"Twoja pozostała kwota kredytu to {credit_value_24:.2f}, to {credit_value_23 - credit_value_24:.2f} mniej niż w poprzednim miesiącu.")

# doczytac o:
# 1. f-strings
# 2. typy zmiennych
# strona RealPython merytorycznie dobra
